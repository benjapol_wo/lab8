package pizzashop.food;

/**
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.10
 */
public interface FoodOrderItem {
	public double getPrice();

	public String toString();
}
